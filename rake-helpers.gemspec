# -*- encoding: utf-8 -*-
$:.unshift File.expand_path("../lib", __FILE__)
require "rake-helpers/version"

Gem::Specification.new do |s|
  s.name          = "rake-helpers"
  s.version       = RakeHelpers::VERSION
  s.authors       = [ "Stephane D'Alu" ]
  s.email         = [ "sdalu@sdalu.com" ]
  s.homepage      = "http://github.com/sdalu/rake-helpers"
  s.summary       = "Various helpers for automation with rake"

  s.add_dependency 'sequel'
  s.add_dependency 'nokogiri'
  s.add_dependency 'sshkit'
  s.add_dependency 'faraday'
  s.add_dependency 'faraday-cookie_jar'

  s.add_development_dependency "yard"
  s.add_development_dependency "rake"
  s.add_development_dependency "redcarpet"

  s.license       = 'Apache-2.0'

  s.files         = %w[ LICENSE Gemfile rake-helpers.gemspec ] + 
		     Dir['lib/**/*.rb'] 
end
