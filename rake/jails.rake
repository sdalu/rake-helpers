JAIL_LIST = [] unless defined? JAIL_LIST

desc "Jail list"
task "jail:list" do
    jail_list(*JAIL_LIST).each {|jid, name, *, path:, ** |
        puts "#{name}: #{jid} #{path}"
    }
end

desc "Jail update"
task "jail:update" do
    jl = jail_list(*JAIL_LIST).each {|jid, name, *, path:, **|
        puts "Jail: #{name}"
        sh "freebsd-update", "-b", path, "fetch"
        if !File.exists? "#{path}/etc/make.conf"
            sh "pkg", "-j", name, "update"
        end
    }
    if jl.empty?
        puts "no running jails"
    end
end

desc "Jail upgrade"
task "jail:upgrade" do
    jail_list(*JAIL_LIST).each {|jid, name, *, path:, **|
        puts "Jail: #{name}"
        # sh "freebsd-update", "-b", path, "install"
        if !File.exists? "#{path}/etc/make.conf"
            sh "pkg", "-j", name, "upgrade"
        end
    }
end
