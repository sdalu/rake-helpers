desc "Ports update"
task "ports:update" do
    sh "make", "-C", "/usr/ports", "update"
    sh "make", "-C", "/usr/ports", "index"    
    sh "portsdb", "-u"
end

desc "Ports upgrade"
task "ports:upgrade" do
    date  = Date.today.to_s
    snaps = zfs :list_snapshots, "system/pkg"
    sh "portupgrade", "-aF"
    if !snaps.include?(date)
        zfs :snapshot, "system/pkg", date, recursive: true
    end
    sh "portupgrade", "-a"
end
