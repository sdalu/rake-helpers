
POUDRIERE_PORTS   ||= [ 'default' ]
POUDRIERE_OPTIONS ||= if defined? POUDRIERE
                          POUDRIERE.map {|p|
                              p.select {|k,v| [ :set, :pkglist ].include?(k) }
                          }
                      else
                          [ ]
                      end
POUDRIERE_BULK    ||= if defined? POUDRIERE
                          POUDRIERE.flat_map {|p|
                              Array(p[:jail])&.map {|j| p.merge(:jail => j) }
                          }.compact
                      else
                          [ ]
                      end


desc "Poudriere ports update"
task "poudriere:ports:update" do
    POUDRIERE_PORTS.each {|tree|
        poudriere_ports :update, tree
    }
end

desc "Poudriere options configuration"
task "poudriere:options:config", [:name, :tags] do |t, args|
    POUDRIERE_OPTIONS.each do |desc|
        jail    = desc.fetch(:jail   ) { nil   }
        tree    = desc.fetch(:tree   ) { nil   }
        set     = desc.fetch(:set    ) { nil   }
        pkglist = desc.fetch(:pkglist) { nil   }
        skip    = desc.fetch(:skip   ) { false }

        next if skip
        next if args[:name]&.!=(set)
        
        filter = if args[:tags]
                     args[:tags].split('|').map {|t|
                         case t
                         when /^:/ then t[1..-1].to_sym
                         else           t
                         end
                     }
                 end
        poudriere_options(*pkglist, jail: jail, tree: tree, set: set,
                          filter: filter)
    end
end

desc "Poudriere bulk build"
task "poudriere:bulk", [:name, :tags] do |t, args|
    POUDRIERE_BULK.each do |desc|
        jail    = desc.fetch(:jail   ) { nil   }
        tree    = desc.fetch(:tree   ) { nil   }
        set     = desc.fetch(:set    ) { nil   }
        pkglist = desc.fetch(:pkglist) { nil   }
        skip    = desc.fetch(:skip   ) { false }

        next if skip
        next if args[:name]&.!=(set)
        filter = if args[:tags]
                     args[:tags].split('|').map {|t|
                         case t
                         when /^:/ then t[1..-1].to_sym
                         else           t
                         end
                     }
                 end
        begin
            poudriere_bulk(*pkglist, jail: jail, tree: tree, set: set,
                           filter: filter)
        rescue RuntimeError => e
            puts "ERROR: #{e}"
        end
    end
end

desc "Poudriere jail list"
task "poudriere:jail:list" do
    puts poudriere_jail_list
end

desc "Poudriere jail Update"
task "poudriere:jail:update" do
    POUDRIERE_JAILS.each {|name|
        poudriere_jail :update, name
    }
end
