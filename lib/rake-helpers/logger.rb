require 'logger'

module RakeHelpers
    @@logger = nil
    
    def self.install_logger(level: :info, timestamp: nil)
        @@logger = $logger = Logger.new($stderr, level: level)
        $logger.formatter = proc {|severity, datetime, progname, msg|
            str = case msg
                  when String     then msg
                  when Exception  then "#{msg.message} (#{msg.class})\n" <<
                                       (msg.backtrace || []).join("\n")
                  else                   msg.inspect
                  end
            
            if timestamp
                "%s [%s]: %s\n" % [ severity[0..0],
                                    datetime.strftime(timestamp),
                                    str
                                  ]
            else
                "%s: %s\n" % [ severity[0..0], str ]
            end
        }
    end

end
