require 'shellwords'

module RakeHelpers
    def x(*args)
        args.map {|s| Shellwords.escape(s) }
    end
end
