require 'rexml/document'

module RakeHelpers
    
    def ps(jail: nil, user: nil, group: nil, pid: nil)
        if respond_to?(:within) && respond_to?(:with) && respond_to?(:as)
            raise "don't us in sshkit context"
        end
        
        cmd = %w[ps axww --libxo=xml,pretty]
        cmd << '-ojid,pid,ppid,user,uid,fib,%cpu,%mem,command'
        cmd << '-J' << jail .to_s if jail
        cmd << '-U' << user .to_s if user
        cmd << '-G' << group.to_s if group
        cmd << '-p' << pid  .to_s if pid
        data = capture(*cmd)
        
        return [] if data.empty?
        
        kmap = { 'jail-id'        => 'jid',
                 'percent-cpu'    => '%cpu',
                 'percent-memory' => '%mem', 
               }
        
        doc = REXML::Document.new(data)
        doc.root.get_elements('process').map {|p|
            Hash[p.elements.map {|e|
                     k, v = e.name, e.text
                     k = kmap.fetch(k) { k }
                     v = case k
                         when 'jid', 'pid', 'ppid', 'uid', 'fib', 'user'
                             v.to_i
                         when '%cpu', '%mem'
                             v.to_f
                         else
                             v
                         end
                     [ k.to_sym, v ]
                 }]
        }
    end

end

