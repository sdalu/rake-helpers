require 'rexml/document'

module RakeHelpers

    def jail_list(hosts: nil)
        lst = []
        cmd = %w[jls -n --libxo xml,pretty]
        doc = REXML::Document.new(capture(*cmd))
        doc.each_element('//jail') {|j|
            jid    = j.elements['jid'          ].text.to_i
            name   = j.elements['name'         ].text
            path   = j.elements['path'         ].text
            parent = j.elements['parent'       ].text.to_i
            host   = j.elements['host.hostname'].text
            lst << [ jid, name, parent, :path => path, :host => host ]
        }
        if hosts
            lst.select! {| *, host:, ** |
                case hosts
                when Regexp then host =~ hosts
                else false
                end
            }
        end
        lst
    end

end
