require 'sshkit'
require 'sshkit/dsl'

module RakeHelpers

    def self.install_sshkit(logfmt: :pretty)
        #
        # SEE: https://github.com/capistrano/sshkit/blob/master/EXAMPLES.md
        #
        
        SSHKit.config.default_runner   = :sequence # Keep it!!!
        SSHKit.config.format           = logfmt
        SSHKit.config.output_verbosity = :info

        include SSHKit::DSL
    end

end
