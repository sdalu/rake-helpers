require 'sequel'
require 'date'


module RakeHelpers
    def self.install_db
        if defined?(Sequel::Mysql)
            Sequel::MySQL.convert_invalid_date_time = nil
        end
    end

    # Retrieve the last date used in a table
    def db_find_last_date(table, field, fallback: nil, &refinement)
        table     = refinement.(table) if refinement
        if row = table.group(field).order(Sequel.desc(field))
                     .select(field).first
            row[field].to_date
        end || (case fallback
                when Date     then fallback
                when String   then Date.parse(fallback)
                when NilClass then nil
                else raise ArgumentError
                end)
    end
    

    # Compute the range of date to fetch according to the database last date
    def db_date_fetching_range(table, field, offset: nil, fallback: nil,
                               &refinement)
        last_date = db_find_last_date(table, field, &refinement)
        max_date  = Date.today
        min_date  = if last_date.nil?
                        case fallback
                        when Date     then fallback
                        when String   then Date.parse(fallback)
                        when NilClass then nil
                        else raise ArgumentError
                        end
                    else
                        last_date + 1
                    end
        return nil if min_date.nil?
        
        case offset
        when Range
            min_date += offset.first
            max_date += offset.last

        when Numeric
            max_date += offset

        when NilClass
            
        else raise ArgumentError, "invalid offset"
        end

        min_date .. max_date
    end
    
end
