require 'tempfile'

module RakeHelpers

    def self.install_poudriere(etc: nil)
        if etc.nil?
        then etc = ENV['POUDRIERE_ETC']
        else ENV['POUDRIERE_ETC'] = etc
        end
        @@poudriere_etc = etc || '/usr/local/etc'
        @@poudriere_d   = @@poudriere_etc + '/poudriere.d'
    end

    
    def poudriere(subcmd, action, *args, **opts)
        case subcmd
        when :jail, :ports, :options, :bulk
            send(:"poudriere_#{subcmd}", action, *args, **opts)
        else raise "unknown poudriere subcommand (#{subcmd})"
        end
    end
    
    def poudriere__pkglist(*pkglist, filter: nil)
        tmpfile = Tempfile.new('pkglist')
        pkglist = Array(pkglist)

        if !filter.nil?
            filter  = Array(filter)
            pkglist &= filter
        end

        from_filepath = ->(filepath) {
            File.readlines(filepath, chomp: true)
                .map {|l| l.gsub(/\s*\#.*$/, '') }
        }
        
        list = pkglist.flat_map {|entry|
            if entry =~ %r{[\w\-]+/[\w\-]+}
                entry
            elsif Symbol === entry
                from_filepath.(@@poudriere_d + '/' + "pkglist/#{entry}.lst")
            elsif File.dirname(file) == '.'
                from_filepath.(@@poudriere_d + '/' + File.basename(file))
            else
                from_filepath.(entry)
            end
        }.map {|e| e.strip }.uniq.reject {|e| e.empty? }
        tmpfile.puts(list)
        tmpfile.flush
        [ '-f', tmpfile.path ]
    end
    
    def poudriere_jail_list
        (capture "poudriere", "jail", "-l", "-n", "-q").split(' ')
    end

    def poudriere_jail(action, name = nil, *args)
        cmd  = [ 'poudriere', 'jail' ]
        
        opts = []
        case action
        when :create  then opts << '-c'
        when :delete  then opts << '-d'
        when :start   then opts << '-s'
        when :kill    then opts << '-k'  << args[0]
        when :rename  then opts << '-r'
        when :update  then opts << '-u'
        else raise "unknown action (#{action}) for poudriere jail"
        end

        sh *cmd, *opts
    end

    def poudriere_ports(action, tree = nil)
        cmd  = [ 'poudriere', 'ports' ]
        opts = []
        
        case action
        when :update
            opts << '-u'
            opts << '-p' << tree if tree
        end
        
        sh *cmd, *opts
    end
    
    def poudriere_options(*pkglist, jail: nil, tree: nil, set: nil, arch: nil,
                          config: nil, filter: nil)
        list = poudriere__pkglist(*pkglist, filter: filter)
        
        cmd  = [ 'poudriere', 'options' ]
        
        opts = []
        opts << '-j' << jail if jail
        opts << '-p' << tree if tree
        opts << '-z' << set  if set
        opts << '-a' << arch if arch
        
        case config
        when true              then opts << '-c'
        when :conditional, nil then opts << '-C'
        when :remove           then opts << '-r'
        end
        
        sh *cmd, *opts, *list
    end
    

    def poudriere_bulk(*pkglist, jail:, tree: nil, set: nil,
                       clean: nil, restricted: nil, filter: nil)
        list = poudriere__pkglist(*pkglist, filter: filter)
        list = [ '-a' ] if list.empty?
        
        cmd  = [ 'poudriere', 'bulk' ]
        
        opts = []
        opts << '-j' << jail if jail
        opts << '-p' << tree if tree
        opts << '-z' << set  if set
        
        case clean
        when true, :all then opts << '-c'
        when :specified then opts << '-C'
        end
        
        opts << '-R' if restricted == false
        
        sh *cmd, *opts, *list
    end
end
