module RakeHelpers

    def zfs(cmd, *args, **opts)
        case cmd
        when :list           then zfs_list(*args, **opts)
        when :snapshot       then zfs_snapshot(*args, **opts)
        when :list_snapshots then zfs_list_snapshots(*args)
        else raise ArgumentError, "unhandled zfs '#{cmd}' command"
        end
    end

    def zfs_snapshot(fs, name, recursive: false)
        cmd = %w[zfs snapshot]
        cmd << '-r' if recursive
        cmd << "#{fs}@#{name}"
        execute(*cmd)
    end

    def zfs_list(fs = nil, type: :fs, recursive: false, depth: nil)
        cmd = %w[zfs list -H -p -o name]
        cmd << '-t'
        cmd << case type
               when :fs                    then :filesystem
               when :filesystem, :snapshot then type
               else raise ArgumentError, "unknown type (#{type})"
               end
        
        if !fs.nil?
            
            if (type == :snapshot) && !recursive 
                depth     = 1
                recursive = true
            end
            
            if recursive
                cmd << '-r'
            end
            if depth
                cmd << '-d' << depth.to_s
            end
            
            cmd << fs
        end
        
        data = capture(*cmd)
        data.split
    end
    
    
    def zfs_list_snapshots(fs)
        return nil if ! (zfs :list, recursive: true).include?(fs)
        (zfs :list, fs, type: :snapshot).map {|e| e.split('@').last }
    end

end
