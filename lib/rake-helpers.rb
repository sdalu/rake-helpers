require 'open3'

module RakeHelpers
    def self.import(*list)
        list.each {|file|
            Rake.application.add_import "#{__dir__}/../rake/#{file}.rake"
        }
    end
    
    def capture(*cmd)
        if Kernel.const_defined?(:SSHKit)
            run_locally do capture(*cmd) end
        else
            data, status = Open3.capture2(*cmd)
            fail(cmd.join(' ')) if !status.exitstatus.zero?
            data
        end
    end
    
    def execute(*cmd)
        if Kernel.const_defined?(:SSHKit)
            run_locally do execute(*cmd) end
        else
            sh(*cmd)
        end
    end


    def os_type
        capture('uname', '-s')
    end

end

